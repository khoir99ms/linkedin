__author__ = 'Khoir MS'

import re
import time
import urlparse

from urllib import quote
from auth import Auth
from utils.general import json_build
from extractor import Profile as ProfileExt, Posts as PostExt, Comments as CommentExt, Search as SearchExt

class LinkedIn(Auth):
    __MAX_LOGIN_ATTEMPTS = 1

    def __init__(self, wait=None, **kwargs):
        super(LinkedIn, self).__init__(wait=wait, **kwargs)
        self.extractor_post = None
        self.extractor_comment = None
        self.extractor_profile = None
        self.extractor_search = None

    def prepare(self, username=None, password=None, cookies=None, sleep=0):
        if not username: raise Exception("username is not defined")
        if not password: raise Exception("password is not defined")

        self.username = username
        self.password = password
        self.cookies = cookies

        attempts = 0
        while not self.login_status and attempts < self.__MAX_LOGIN_ATTEMPTS:
            self.authentication(sleep=sleep)
            attempts += 1

    def search(self, query='', location=None, max_page=1, **kwargs):
        found = 0
        self.extractor_search = SearchExt()

        try:
            search_url = 'https://www.linkedin.com/search/results/people/?keywords={}&origin=SWITCH_SEARCH_VERTICAL'.format(query)
            self.load_and_wait(search_url, '.search-results-page')

            result_check = '.search-results-page .search-no-results'
            if not self.wait_by_css_selector(selector=result_check, wait=1):
                self.__normalize_filter()
                self.__set_location(location)

                n = 0
                while n < max_page:
                    if self.wait_by_css_selector(selector=result_check, wait=1):
                        break

                    self.scrolling_to_element(selector='.page-list', retry=2)

                    html = self.get_element('.results-list')
                    if html:
                        html = html.get_attribute('outerHTML')
                        self.extractor_search.set_document(html)

                        peoples = self.extractor_search.profile()
                        peoples['count'] = len(peoples.get('profile', []))
                        peoples['page'] = n + 1

                        found += peoples['count']
                        yield peoples

                    n += 1
                    self.log('page {} processed'.format(n))
                    if n < max_page:
                        self.click_next(selector='li.active + li > button', limit=1, wait=1)

            self.log('found {} people'.format(found))
        except:
            self.log(level='error')
            raise

    def profiling(self, user_id, detail=False):
        result = {}
        self.extractor_profile = ProfileExt()
        try:
            profile_url = user_id if user_id.startswith('http') else "https://www.linkedin.com/in/{}".format(user_id)
            self.load_and_wait(profile_url, '#profile-wrapper')

            # get user id
            uri_path = urlparse.urlsplit(profile_url).path
            uri_path = filter(lambda v:v, uri_path.split('/'))
            if len(uri_path) == 2:
                result['user_id'] = uri_path[1]
            else:
                result['user_id'] = user_id

            # basic profile action
            self.click_selector('button[data-control-name="contact_see_more"]')
            self.click_selector('.button-tertiary-small[aria-expanded="false"]')
            html = self.get_element('html').get_attribute('outerHTML')
            self.extractor_profile.set_document(doc=html)
            result.update(self.extractor_profile.basic())

            if detail:
                result.update(self.profile_experience())
                result.update(self.profile_education())
                result.update(self.profile_volunteer())
                result.update(self.profile_skills())
                result.update(self.profile_accomplishments())

            return result
        except:
            self.log(level='error')
            raise

    def profile_experience(self):
        # experience profile action
        self.scrolling_to_element(selector='.pv-profile-section.experience-section', retry=2)
        self.click_next('.experience-section .pv-profile-section__see-more-inline', limit=20)
        self.click_multiple('.experience-section .pv-profile-section__show-more-detail[aria-expanded="false"]')
        html = self.get_element('html').get_attribute('outerHTML')
        self.extractor_profile.set_document(doc=html)
        experiences = self.extractor_profile.experience()

        return experiences

    def profile_education(self):
        # education profile action
        self.scrolling_to_element(selector='.pv-profile-section.education-section', retry=2)
        self.click_next('.education-section .pv-profile-section__see-more-inline', limit=20)
        self.click_multiple('.education-section .pv-profile-section__show-more-detail[aria-expanded="false"]')
        html = self.get_element('html').get_attribute('outerHTML')
        self.extractor_profile.set_document(doc=html)
        educations = self.extractor_profile.education()

        return educations

    def profile_volunteer(self):
        # volunteer profile action
        self.scrolling_to_element(selector='.pv-profile-section.volunteering-section', retry=2)
        self.click_next('.volunteering-section .pv-profile-section__see-more-inline', limit=20)
        self.click_multiple('.volunteering-section .pv-profile-section__show-more-detail[aria-expanded="false"]')
        html = self.get_element('html').get_attribute('outerHTML')
        self.extractor_profile.set_document(doc=html)
        volunteers = self.extractor_profile.volunteer()

        return volunteers

    def profile_skills(self):
        # skills profile action
        self.scrolling_to_element(selector='.pv-profile-section.pv-featured-skills-section', retry=2)
        self.click_next('.pv-skills-section__additional-skills[aria-expanded="false"]', limit=20)
        html = self.get_element('html').get_attribute('outerHTML')
        self.extractor_profile.set_document(doc=html)
        skills = self.extractor_profile.skill()

        return skills

    def profile_accomplishments(self):
        result = {}

        html = []
        accomplishments = self.get_elements('.pv-profile-section.pv-accomplishments-block')
        for i, accomplishment in enumerate(accomplishments):
            if i > 0:
                self.move_to_element(accomplishments[i - 1])
            button = self.get_element_from(accomplishment, 'button[data-control-name^="accomplishments_expand"]')
            self.click(button, accomplishment)
            time.sleep(0.5)

            while True:
                button_more = self.get_element_from(accomplishment, 'button.pv-profile-section__see-more-inline.link')
                if not button_more:
                    break
                self.click(button_more, accomplishment)
                time.sleep(1.5)
            html.append(accomplishment.get_attribute('outerHTML'))

        if html:
            html = '\n'.join(html)
            self.extractor_profile.set_document(doc=html, parser='soup')
            courses = self.extractor_profile.course()
            result.update(courses)

            projects = self.extractor_profile.project()
            result.update(projects)

            publications = self.extractor_profile.publication()
            result.update(publications)

            awards = self.extractor_profile.award()
            result.update(awards)

            languages = self.extractor_profile.language()
            result.update(languages)

            organizations = self.extractor_profile.organization()
            result.update(organizations)

            certifications = self.extractor_profile.certification()
            result.update(certifications)

            patents = self.extractor_profile.patent()
            result.update(patents)

            scores = self.extractor_profile.score()
            result.update(scores)

        return result

    def posts(self, user_id=None, query=None, max_page=1, max_id=None, simple_post=True):
        url = None
        ids = set()

        try:
            self.__mode = "POST_SIMPLE"
            if user_id:
                url = user_id if user_id.startswith('http') else "https://www.linkedin.com/in/{}/recent-activity/shares/".format(user_id)
            if query:
                url = "https://www.linkedin.com/search/results/content/?keywords={}".format(quote(query))
            if not url:
                raise Exception("user id or query is not defined")
            if not self.browser.current_url == url:
                self.load_and_wait(url, '#detail-recent-activity, .search-results-page')
            if not simple_post: self.__mode = "POSTS"
            if max_id:
                self.scrolling_down_until(selector='article[data-id^="urn:li"][data-id$="{}"]'.format(max_id))

            n = 0
            after_max_id_found = False
            self.extractor_post = PostExt()
            while n < max_page:
                self.scroll_down()
                time.sleep(1)
                html = self.get_element('html').get_attribute('outerHTML')
                self.extractor_post.set_document(html)

                for data in self.extractor_post.post(self.__mode):
                    data['id'] = re.sub(r'[^0-9]', '', data['url'])
                    if not data['id'] in ids:
                        data['url'] = "https://www.linkedin.com/feed/update/{}".format(data['url'])
                        data = json_build(data)
                        try:
                            if max_id:
                                if after_max_id_found:
                                    yield data
                                if not after_max_id_found:
                                    after_max_id_found = bool(
                                        re.search(r'{}$'.format(max_id), data['id'], flags=re.UNICODE))
                            else:
                                yield data
                        except GeneratorExit:
                            return
                        ids.add(data['id'])
                n += 1
        except:
            self.log(level='error')
            raise

    def post_detail(self, post_id):
        result = {}
        self.extractor_post = PostExt()
        try:
            self.__mode = "POST_DETAIL"
            post_url = post_id if post_id.startswith('http') else "https://www.linkedin.com/feed/update/{}/".format(post_id)
            self.load_and_wait(post_url, 'article.feed-s-update')

            html = self.get_element('html').get_attribute('outerHTML')
            self.extractor_post.set_document(html)
            for data in self.extractor_post.post(self.__mode):
                data['id'] = re.sub(r'[^0-9]', '', post_id)
                data['url'] = post_url
                result.update(data)
            return json_build(result)
        except:
            self.log(level='error')
            raise

    def comments(self, post_id):
        ids = set()
        __next = 0

        try:
            post_url = post_id if post_id.startswith('http') else "https://www.linkedin.com/feed/update/{}/".format(post_id)
            if not self.browser.current_url == post_url:
                self.load_and_wait(post_url, '.feed-s-comments-list')
            self.extractor_comment = CommentExt()
            self.__hide_overlay()

            btn_next = 'button#show_prev'
            while True:
                html = self.get_element('html').get_attribute('outerHTML')
                self.extractor_comment.set_document(html)

                for data in self.extractor_comment.comment():
                    if not data['comment_id'] in ids:
                        __next = 0
                        ids.add(data['comment_id'])
                    else:
                        break

                    data['url'] = post_url
                    data['post_id'] = re.sub(r'[^0-9]', '', post_id)

                    yield json_build(data)

                if self.wait_by_css_selector(selector=btn_next, wait=1):
                    self.click_next(selector=btn_next, limit=1, wait=1)
                    time.sleep(0.5)
                else:
                    break

                __next += 1
                if __next > 9:
                    break

        except:
            self.log(level='error')
            raise

    def __normalize_filter(self):
        self.log('reset filter search')
        btn_reset_locator = 'button.search-filters__reset'
        btn_reset = self.get_element(btn_reset_locator)
        while btn_reset:
            btn_reset.click()
            time.sleep(3)
            btn_reset = self.get_element(btn_reset_locator)

    def __set_location(self, location=None):
        if not location: return

        self.log('set location {}'.format(location))
        location_container = '.search-facet--geo-region'
        btn_expand = '{} > button'.format(location_container)
        btn_add_location = '{} button.search-s-add-facet__button'.format(location_container)

        self.wait_by_css_selector(selector=btn_expand, what='show', wait=60).click()
        self.wait_by_css_selector(selector=btn_add_location, what='show', wait=60).click()

        input_location = self.get_element('{} li.search-s-add-facet > section input[role="combobox"]'.format(location_container))
        input_location.clear()
        input_location.send_keys(location)
        time.sleep(0.5)
        self.click_selector('{} ul.type-ahead-results > li'.format(location_container))
        time.sleep(2)

    def __hide_overlay(self):
        try:
            overlay = self.get_element('#msg-overlay')
            if overlay:
                self.browser.execute_script("arguments[0].style.visibility='hidden'", overlay)
            return True
        except:
            return False

if __name__ == '__main__':
    import json
    # c = LinkedIn()
    c = LinkedIn(driver='chrome', driver_path='/opt/chromedriver/2.32/chromedriver')
    cookies = [{u'domain': u'.linkedin.com', u'name': u'lang', u'value': u'"v=2&lang=en-us"', u'path': u'/', u'httpOnly': False, u'secure': False}, {u'domain': u'www.linkedin.com', u'secure': False, u'value': u'"v=1&M"', u'expiry': 1567850715.752992, u'path': u'/', u'httpOnly': False, u'name': u'visit'}, {u'domain': u'.linkedin.com', u'secure': False, u'value': u'CwEAAAFeW8z5k6bZvGDhM6YlkL9td-v1ULoxCmhGDodT6xq1nwzThZlMu8eHJZhva8Mi2QdysIk6yqrbw6BZhFnviaKLSFEGpHe5evwExfGubtH5k93AkbfV2inH9--5AxU4yPV8j00leVbkO9OmNoBxHDVXy_1jwsX4n9A564-wNwKewvlZEG0PuaR0SrQSZlOeF2tA3527NRdMBcj0dNRKrskCzBtdWEayTSYEITAWwKr7V14Z3VPS6xoewdtbPWtQkTA', u'expiry': 1507370714.53126, u'path': u'/', u'httpOnly': False, u'name': u'_lipt'}, {u'domain': u'.linkedin.com', u'secure': False, u'value': u'GA1.2.1292918546.1496653327', u'expiry': 1567850713, u'path': u'/', u'httpOnly': False, u'name': u'_ga'}, {u'domain': u'.www.linkedin.com', u'name': u'JSESSIONID', u'value': u'"ajax:0181062224133637856"', u'path': u'/', u'httpOnly': False, u'secure': False}, {u'domain': u'.linkedin.com', u'name': u'sdsc', u'value': u'1%3A1SZM1shxDNbLt36wZwCgPgvN58iw%3D', u'path': u'/', u'httpOnly': False, u'secure': False}, {u'domain': u'.linkedin.com', u'secure': False, u'value': u'"b=SB40:g=39:u=107:i=1504778711:t=1504846118:s=AQElKipwjHox_9o7Gp_i1lqj-d2-MQdt"', u'expiry': 1504846118.23339, u'path': u'/', u'httpOnly': False, u'name': u'lidc'}, {u'domain': u'.linkedin.com', u'name': u'_gat', u'value': u'1', u'path': u'/', u'httpOnly': False, u'secure': False}, {u'domain': u'.linkedin.com', u'name': u'bcookie', u'value': u'"v=2&9de5c66f-4a73-4ca6-83b1-b54ae422d92a"', u'path': u'/', u'httpOnly': False, u'secure': False}, {u'domain': u'.www.linkedin.com', u'name': u'li_at', u'value': u'AQEDASDo2LQD7bhGAAABXlarAHgAAAFeereEeE4Adjb8ne1TSkLE5N4hQJBRqoegq27Z7cBDfA7vTBz-8kc5a-FJkzu7sShF7P5VJR1-_pP3mPXUi45CP88ua0H43VzLo5xh-2dadvnaoaFlJZi_0eLR', u'path': u'/', u'httpOnly': False, u'secure': False}, {u'domain': u'.www.linkedin.com', u'name': u'visit', u'value': u'"v=1&G"', u'path': u'/', u'httpOnly': False, u'secure': False}, {u'domain': u'.linkedin.com', u'name': u'liap', u'value': u'true', u'path': u'/', u'httpOnly': False, u'secure': False}, {u'domain': u'.www.linkedin.com', u'name': u'sl', u'value': u'"v=1&n9TEq"', u'path': u'/', u'httpOnly': False, u'secure': False}, {u'domain': u'.www.linkedin.com', u'name': u'bscookie', u'value': u'"v=1&2017090610095147ae1c18-6548-475f-8e97-8dbf12e8ab0bAQEK-pdlRKnPKXlORnBb4RFALRpJJM-r"', u'path': u'/', u'httpOnly': False, u'secure': False}, {u'domain': u'.linkedin.com', u'secure': False, u'value': u's=1504778711253&r=https%3A%2F%2Fwww.linkedin.com%2F', u'expiry': 1504779311, u'path': u'/', u'httpOnly': False, u'name': u'RT'}]
    c.prepare(username="clipper.monitoring4@gmail.com", password="Rahasia123", cookies=cookies, sleep=3)
    print c.browser.get_cookies()
    start = time.time()
    # print c.profiling(user_id='arif-syarifudin-3a07515a', detail=False)
    # print c.profiling(user_id='toxtli', detail=True)Return in generator together
    print c.browser.current_url
    max_id = ''
    # for d in c.posts(user_id='arif-syarifudin-3a07515a', max_id=max_id, max_page=1, simple_post=False):
    #     max_id = d['id']
    #     print max_id
    #     print json.dumps(d, indent=4)
    # for d in c.posts(query="dede", max_page=1, simple_post=True):
    #     print json.dumps(d, indent=4)
    # print json.dumps(c.post_detail(post_id='urn:li:activity:6246724981863092224'), indent=4)
    # print c.post_detail(post_id='urn:li:activity:6246683103390195712')
    for d in c.comments(post_id='urn:li:article:8862711136887026984'):
        print json.dumps(d, indent=4)

    # print json.dumps(c.profiling(user_id='toxtli', detail=True), indent=4)
    # print json.dumps(c.profile_experience(), indent=4)
    # print json.dumps(c.profile_education(), indent=4)
    # print json.dumps(c.profile_volunteer(), indent=4)
    # print json.dumps(c.profile_skills(), indent=4)
    # print json.dumps(c.profile_accomplishments(), indent=4)
    # print json.dumps(c.profiling(user_id='arif-syarifudin-3a07515a', detail=True), indent=4)

    # print json.dumps(c.search(query='james', max_page=1), indent=4)
    # print json.dumps(c.search(location='bandung', max_page=1000), indent=4)
    # for p in c.search(location='jakarta', max_page=1):
    #     print json.dumps(p, indent=4)
    end = time.time() - start
    print "Elapsed time : %f" % end