__author__ = 'Khoir MS'

import time
import pickle

from browser import Browser

class Auth(Browser):

    def __init__(self, wait=0, **kwargs):
        super(Auth, self).__init__(**kwargs)

        self.baseurl = 'https://www.linkedin.com'
        self.login_url = 'https://www.linkedin.com/uas/login'

        self.robot = {}
        self.cookies = None
        self.username = None
        self.password = None
        self.login_status = False

        if wait:
            self.WAIT = wait

    def authentication(self, sleep=0):
        try:
            if self.cookies:
                self.browser.get(self.baseurl)
                self.log('login with cookies')
                if self.set_cookies():
                    self.browser.refresh()
                    time.sleep(sleep)
                    if not self.__is_logged_in():
                        self.log('failed login with cookies')
                        self.login(sleep=sleep)
                    else:
                        self.login_status = True
                else:
                    self.log('failed login with cookies')
                    self.login(sleep=sleep)
            else:
                self.login(sleep=sleep)

            if self.login_status:
                self.log('logged in, robot is ready to use')
            else:
                raise Exception('sorry, robot is not ready to use')
        except:
            raise

    def login(self, username=None, password=None, sleep=0):
        self.log('login with username and password')
        self.browser.get(self.login_url)

        if username:
            self.username = username
        if password:
            self.password = password

        if not self.username or not self.password:
            raise Exception('username or password not defined')

        self.browser.find_element_by_id('session_key-login').clear()
        self.browser.find_element_by_id('session_key-login').send_keys(self.username)
        self.browser.find_element_by_id('session_password-login').clear()
        self.browser.find_element_by_id('session_password-login').send_keys(self.password)
        self.browser.find_element_by_id('btn-primary').click()

        time.sleep(sleep)

        self.login_status = self.__is_logged_in()

    def __is_logged_in(self, wait=0):
        if wait:
            self.WAIT = wait
        what = '#voyager-feed'

        return self.wait_by_css_selector(selector=what, wait=self.WAIT)

    def __logout(self):
        pass

    def set_cookies(self, cookies=None):
        try:
            if cookies:
                self.cookies = cookies

            if isinstance(self.cookies, str) or isinstance(self.cookies, unicode):
                self.cookies = pickle.loads(self.cookies)

            self.verify_cookie()
            self.browser.delete_all_cookies()
            if isinstance(self.cookies, list):
                for c in self.cookies:
                    cookie = "document.cookie = '{name}={value}; path={path}; domain={domain}; expiry={expiry}; expires={expires}';".format(**c)
                    self.browser.execute_script(cookie)
                return True
            elif isinstance(self.cookies, dict):
                cookie = "document.cookie = '{name}={value}; path={path}; domain={domain}; expiry={expiry}; expires={expires}';".format(**self.cookies)
                self.browser.execute_script(cookie)
                return True
            else:
                return False
        except:
            self.log(level='error')
            return False

    def verify_cookie(self):
        if isinstance(self.cookies, dict):
            self.cookies = [self.cookies]

        cookies = list()
        for cookie in self.cookies:
            if 'name' not in cookie:
                cookie['name'] = None
            if 'value' not in cookie:
                cookie['value'] = None
            if 'path' not in cookie:
                cookie['path'] = None
            if 'domain' not in cookie:
                cookie['domain'] = None
            if 'expiry' not in cookie:
                cookie['expiry'] = None
            if 'expires' not in cookie:
                cookie['expires'] = None
            cookies.append(cookie)
        self.cookies = cookies