ARTICLE_SHARE_CONTAINER = '.feed-s-update__update-content-wrapper'
# CONTAINER = {'POSTS': '#detail-recent-activity, .search-results-page', 'POST_DETAIL': 'article.feed-s-update'}

SECTIONS = {'POST_SIMPLE': {}, 'POSTS': {}, 'POST_DETAIL': {}}
SECTIONS['POST_SIMPLE']['POST'] = {'selector': '#voyager-feed > article, .search-results-page ul.results-list > li article.feed-s-update', 'quantity': 'multiple'}

FIELDS = {'POST_SIMPLE': {}}
FIELDS['POST_SIMPLE']['POST'] = {}
FIELDS['POST_SIMPLE']['POST']['URL'] = {'selector': '', 'type': 'attr', 'attr': 'data-id'}
FIELDS['POST_SIMPLE']['POST']['DATE'] = {'selector': 'time.feed-s-post-meta__timestamp', 'type': 'text'}

SECTIONS['POSTS']['POST'] = {'selector': '#voyager-feed > article, .search-results-page ul.results-list > li article.feed-s-update', 'quantity': 'multiple'}

FIELDS['POSTS'] = {}
FIELDS['POSTS']['POST'] = {}
FIELDS['POSTS']['POST']['URL'] = {'selector': '', 'type': 'attr', 'attr': 'data-id'}
FIELDS['POSTS']['POST']['DATE'] = {'selector': 'time.feed-s-post-meta__timestamp', 'type': 'text'}
FIELDS['POSTS']['POST']['IMAGE'] = {'selector': 'div.feed-s-hero-entity__image', 'type': 'attr', 'attr': 'style'}
FIELDS['POSTS']['POST']['CONTENT'] = {'selector': '.feed-s-main-content', 'type': 'text'}
FIELDS['POSTS']['POST']['ACTOR_URL'] = {'selector': '.feed-s-post-meta__profile-link:eq(0)', 'type': 'attr', 'attr': 'href'}
FIELDS['POSTS']['POST']['ACTOR_NAME'] = {'selector': '.feed-s-post-meta__profile-link .feed-s-post-meta__name:eq(0)', 'type': 'text'}
FIELDS['POSTS']['POST']['SHARE_CONTENT'] = {'selector': '{} .feed-s-main-content'.format(ARTICLE_SHARE_CONTAINER), 'type': 'text'}
FIELDS['POSTS']['POST']['SHARE_ACTOR_URL'] = {'selector': '{} .feed-s-post-meta__profile-link'.format(ARTICLE_SHARE_CONTAINER), 'type': 'attr', 'attr': 'href'}
FIELDS['POSTS']['POST']['SHARE_ACTOR_NAME'] = {'selector': '{} .feed-s-post-meta__profile-link .feed-s-post-meta__name'.format(ARTICLE_SHARE_CONTAINER), 'type': 'text'}
FIELDS['POSTS']['POST']['SHARE_ACTOR_HEADLINE'] = {'selector': '{} .feed-s-post-meta__profile-link .feed-s-post-meta__headline'.format(ARTICLE_SHARE_CONTAINER), 'type': 'text'}
FIELDS['POSTS']['POST']['ARTICLE_SHARE_URL'] = {'selector': '{} .feed-s-hero-entity__image-container a'.format(ARTICLE_SHARE_CONTAINER), 'type': 'attr', 'attr': 'href'}
FIELDS['POSTS']['POST']['ARTICLE_SHARE_HEADLINE'] = {'selector': '{} .feed-s-image-description__headline'.format(ARTICLE_SHARE_CONTAINER), 'type': 'text'}

SECTIONS['POST_DETAIL']['POST'] = {'selector': 'article.feed-s-update', 'quantity': 'multiple'}
FIELDS['POST_DETAIL'] = {}
FIELDS['POST_DETAIL']['POST'] = {}
FIELDS['POST_DETAIL']['POST']['DATE'] = {'selector': 'time.feed-s-post-meta__timestamp', 'type': 'text'}
FIELDS['POST_DETAIL']['POST']['IMAGE'] = {'selector': 'div.feed-s-hero-entity__image', 'type': 'attr', 'attr': 'style'}
FIELDS['POST_DETAIL']['POST']['CONTENT'] = {'selector': '.feed-s-main-content', 'type': 'text'}
FIELDS['POST_DETAIL']['POST']['ACTOR_URL'] = {'selector': '.feed-s-post-meta__profile-link:eq(0)', 'type': 'attr', 'attr': 'href'}
FIELDS['POST_DETAIL']['POST']['ACTOR_NAME'] = {'selector': '.feed-s-post-meta__profile-link:eq(0) .feed-s-post-meta__name', 'type': 'text'}
FIELDS['POST_DETAIL']['POST']['SHARE_CONTENT'] = {'selector': '{} .feed-s-main-content'.format(ARTICLE_SHARE_CONTAINER), 'type': 'text'}
FIELDS['POST_DETAIL']['POST']['SHARE_ACTOR_URL'] = {'selector': '{} .feed-s-post-meta__profile-link'.format(ARTICLE_SHARE_CONTAINER), 'type': 'attr', 'attr': 'href'}
FIELDS['POST_DETAIL']['POST']['SHARE_ACTOR_NAME'] = {'selector': '{} .feed-s-post-meta__profile-link .feed-s-post-meta__name'.format(ARTICLE_SHARE_CONTAINER), 'type': 'text'}
FIELDS['POST_DETAIL']['POST']['SHARE_ACTOR_HEADLINE'] = {'selector': '{} .feed-s-post-meta__profile-link .feed-s-post-meta__headline'.format(ARTICLE_SHARE_CONTAINER), 'type': 'text'}
FIELDS['POST_DETAIL']['POST']['ARTICLE_SHARE_URL'] = {'selector': '{} .feed-s-hero-entity__image-container a'.format(ARTICLE_SHARE_CONTAINER), 'type': 'attr', 'attr': 'href'}
FIELDS['POST_DETAIL']['POST']['ARTICLE_SHARE_HEADLINE'] = {'selector': '{} .feed-s-image-description__headline'.format(ARTICLE_SHARE_CONTAINER), 'type': 'text'}
