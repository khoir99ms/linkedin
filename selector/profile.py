CONTAINER = {'PROFILE': '#profile-wrapper', 'ACCOMPLISHMENT': '.pv-profile-section.pv-accomplishments-block'}

SECTIONS = {'BASIC_PROFILE': {}}
FIELDS = {'BASIC_PROFILE': {}}
# BASIC PROFILE
SECTIONS['BASIC_PROFILE']['NAME'] = {'selector': '.pv-top-card-section__name', 'type': 'text', 'quantity': 'single'}
SECTIONS['BASIC_PROFILE']['IMAGE'] = {'selector': 'img.pv-top-card-section__image', 'type': 'attr', 'attr': 'src', 'quantity': 'single'}
SECTIONS['BASIC_PROFILE']['CONNECTIONS'] = {'selector': '.pv-top-card-section__connections span[aria-hidden]', 'type': 'text', 'quantity': 'single'}
SECTIONS['BASIC_PROFILE']['TITLE'] = {'selector': '.pv-top-card-section__headline', 'type': 'text', 'quantity': 'single'}
SECTIONS['BASIC_PROFILE']['LOCATION'] = {'selector': '.pv-top-card-section__location', 'type': 'text', 'quantity': 'single'}
SECTIONS['BASIC_PROFILE']['BIRTHDAY'] = {'selector': '.pv-contact-info__contact-type.ci-birthday .pv-contact-info__ci-container', 'type': 'text', 'quantity': 'single'}
SECTIONS['BASIC_PROFILE']['PROFILE_URL'] = {'selector': '.pv-contact-info__contact-type.ci-vanity-url .pv-contact-info__ci-container', 'type': 'text', 'quantity': 'single'}
SECTIONS['BASIC_PROFILE']['SUMMARY'] = {'selector': '.pv-top-card-section__summary', 'type': 'text', 'quantity': 'single'}
SECTIONS['BASIC_PROFILE']['FOLLOWERS'] = {'selector': '.pv-recent-activity-section__follower-count', 'type': 'text', 'quantity': 'single'}
SECTIONS['BASIC_PROFILE']['EMAIL'] = {'selector': '.pv-contact-info__contact-type.ci-email .pv-contact-info__ci-container', 'type': 'text', 'quantity': 'single'}

SECTIONS['BASIC_PROFILE']['IMS'] = {'selector': '.pv-contact-info__contact-type.ci-ims li', 'quantity': 'multiple'}
FIELDS['BASIC_PROFILE']['IMS'] = {}
FIELDS['BASIC_PROFILE']['IMS']['IM'] = {'selector': 'span', 'type': 'text'}

SECTIONS['BASIC_PROFILE']['PHONES'] = {'selector': '.pv-contact-info__contact-type.ci-phone li', 'quantity': 'multiple'}
FIELDS['BASIC_PROFILE']['PHONES'] = {}
FIELDS['BASIC_PROFILE']['PHONES']['PHONE'] = {'selector': '', 'type': 'text'}

SECTIONS['BASIC_PROFILE']['WEBSITES'] = {'selector': '.pv-contact-info__contact-type.ci-websites li', 'quantity': 'multiple'}
FIELDS['BASIC_PROFILE']['WEBSITES'] = {}
FIELDS['BASIC_PROFILE']['WEBSITES']['NAME'] = {'selector': 'a', 'type': 'text'}
FIELDS['BASIC_PROFILE']['WEBSITES']['URL'] = {'selector': 'a', 'type': 'attr', 'attr': 'href'}

SECTIONS['BASIC_PROFILE']['TWITTER'] = {'selector': '.pv-contact-info__contact-type.ci-twitter li', 'quantity': 'multiple'}
FIELDS['BASIC_PROFILE']['TWITTER'] = {}
FIELDS['BASIC_PROFILE']['TWITTER']['NAME'] = {'selector': 'a', 'type': 'text'}
FIELDS['BASIC_PROFILE']['TWITTER']['URL'] = {'selector': 'a', 'type': 'attr', 'attr': 'href'}

# DETAIL PROFILE
SECTIONS['PROFILE'] = SECTIONS['BASIC_PROFILE'].copy()
FIELDS['PROFILE'] = FIELDS['BASIC_PROFILE'].copy()

SECTIONS['PROFILE']['EXPERIENCE'] = {'selector': '.pv-profile-section.experience-section li', 'quantity': 'multiple'}
FIELDS['PROFILE']['EXPERIENCE'] = {}
FIELDS['PROFILE']['EXPERIENCE']['TITLE'] = {'selector': '.pv-entity__summary-info h3', 'type': 'text'}
FIELDS['PROFILE']['EXPERIENCE']['IMG'] = {'selector': '.pv-entity__logo.company-logo img:not([class*=ghost])', 'type': 'attr', 'attr': 'src'}
FIELDS['PROFILE']['EXPERIENCE']['COMPANY'] = {'selector': '.pv-entity__secondary-title', 'type': 'text'}
FIELDS['PROFILE']['EXPERIENCE']['COMPANY_URL'] = {'selector': 'a[data-control-name=background_details_company]', 'type': 'attr', 'attr': 'href'}
FIELDS['PROFILE']['EXPERIENCE']['DATE'] = {'selector': '.pv-entity__date-range', 'type': 'text'}
FIELDS['PROFILE']['EXPERIENCE']['DESCRIPTION'] = {'selector': '.pv-entity__description', 'type': 'text'}

SECTIONS['PROFILE']['VOLUNTEER'] = {'selector': '#volunteering-section li', 'quantity': 'multiple'}
FIELDS['PROFILE']['VOLUNTEER'] = {}
FIELDS['PROFILE']['VOLUNTEER']['TITLE'] = {'selector': '.pv-entity__summary-info h3', 'type': 'text'}
FIELDS['PROFILE']['VOLUNTEER']['COMPANY'] = {'selector': '.pv-entity__secondary-title', 'type': 'text'}
FIELDS['PROFILE']['VOLUNTEER']['DATE'] = {'selector': '.pv-entity__date-range', 'type': 'text'}
FIELDS['PROFILE']['VOLUNTEER']['CAUSE'] = {'selector': '.pv-entity__cause', 'type': 'text'}
FIELDS['PROFILE']['VOLUNTEER']['DESCRIPTION'] = {'selector': '.pv-entity__description', 'type': 'text'}

SECTIONS['PROFILE']['PUBLICATIONS'] = {'selector': '.pv-profile-section.publications.pv-accomplishments-block--expanded ul > li', 'quantity': 'multiple'}
FIELDS['PROFILE']['PUBLICATIONS'] = {}
FIELDS['PROFILE']['PUBLICATIONS']['NAME'] = {'selector': 'h4.pv-accomplishment-entity__title', 'type': 'text'}
FIELDS['PROFILE']['PUBLICATIONS']['URL'] = {'selector': 'a.pv-accomplishment-entity__external-source', 'type': 'attr', 'attr': 'href'}
FIELDS['PROFILE']['PUBLICATIONS']['PUBLISHER'] = {'selector': '.pv-accomplishment-entity__publisher', 'type': 'text'}
FIELDS['PROFILE']['PUBLICATIONS']['DATE'] = {'selector': '.pv-accomplishment-entity__date', 'type': 'text'}
FIELDS['PROFILE']['PUBLICATIONS']['DESCRIPTION'] = {'selector': '.pv-accomplishment-entity__description', 'type': 'html'}
FIELDS['PROFILE']['PUBLICATIONS']['CONTRIBUTOR_URL'] = {'selector': 'a[data-control-name="publication_contributors"]', 'type': 'attr', 'attr': 'href'}

SECTIONS['PROFILE']['COURSES'] = {'selector': '.pv-profile-section.courses.pv-accomplishments-block--expanded ul > li', 'quantity': 'multiple'}
FIELDS['PROFILE']['COURSES'] = {}
FIELDS['PROFILE']['COURSES']['NAME'] = {'selector': 'h4.pv-accomplishment-entity__title', 'type': 'text'}

SECTIONS['PROFILE']['PROJECTS'] = {'selector': '.pv-profile-section.projects.pv-accomplishments-block--expanded ul > li', 'quantity': 'multiple'}
FIELDS['PROFILE']['PROJECTS'] = {}
FIELDS['PROFILE']['PROJECTS']['NAME'] = {'selector': 'h4.pv-accomplishment-entity__title', 'type': 'text'}
FIELDS['PROFILE']['PROJECTS']['URL'] = {'selector': 'a.pv-accomplishment-entity__external-source', 'type': 'attr', 'attr': 'href'}
FIELDS['PROFILE']['PROJECTS']['DATE'] = {'selector': '.pv-accomplishment-entity__date', 'type': 'text'}
FIELDS['PROFILE']['PROJECTS']['DESCRIPTION'] = {'selector': '.pv-accomplishment-entity__description', 'type': 'html'}
FIELDS['PROFILE']['PROJECTS']['CONTRIBUTOR_URL'] = {'selector': 'a[data-control-name="project_contributors"]', 'type': 'attr', 'attr': 'href'}

SECTIONS['PROFILE']['AWARDS'] = {'selector': '.pv-profile-section.honors.pv-accomplishments-block--expanded ul > li', 'quantity': 'multiple'}
FIELDS['PROFILE']['AWARDS'] = {}
FIELDS['PROFILE']['AWARDS']['NAME'] = {'selector': '.pv-accomplishment-entity__title', 'type': 'text'}
FIELDS['PROFILE']['AWARDS']['ISSUER'] = {'selector': '.pv-accomplishment-entity__issuer', 'type': 'text'}
FIELDS['PROFILE']['AWARDS']['DATE'] = {'selector': '.pv-accomplishment-entity__date', 'type': 'text'}
FIELDS['PROFILE']['AWARDS']['DESCRIPTION'] = {'selector': '.pv-accomplishment-entity__description', 'type': 'html'}

SECTIONS['PROFILE']['LANGUAGES'] = {'selector': '.pv-profile-section.languages.pv-accomplishments-block--expanded ul > li', 'quantity': 'multiple'}
FIELDS['PROFILE']['LANGUAGES'] = {}
FIELDS['PROFILE']['LANGUAGES']['NAME'] = {'selector': 'h4', 'type': 'text'}
FIELDS['PROFILE']['LANGUAGES']['LEVEL'] = {'selector': '.pv-accomplishment-entity__proficiency', 'type': 'text'}

SECTIONS['PROFILE']['SKILLS'] = {'selector': 'li.pv-skill-entity--featured .tooltip-container',
                                 'quantity': 'multiple'}
FIELDS['PROFILE']['SKILLS'] = {}
FIELDS['PROFILE']['SKILLS']['NAME'] = {'selector': '.pv-skill-entity__skill-name', 'type': 'text'}
FIELDS['PROFILE']['SKILLS']['ENDORSEMENTS'] = {'selector': '.pv-skill-entity__endorsement-count', 'type': 'text'}
FIELDS['PROFILE']['SKILLS']['URL'] = {'selector': 'a.featured-skill-entity-wrapper', 'type': 'attr', 'attr': 'href'}

SECTIONS['PROFILE']['EDUCATION'] = {'selector': '#education-section li', 'quantity': 'multiple'}
FIELDS['PROFILE']['EDUCATION'] = {}
FIELDS['PROFILE']['EDUCATION']['NAME'] = {'selector': '.pv-entity__school-name', 'type': 'text'}
FIELDS['PROFILE']['EDUCATION']['URL'] = {'selector': 'a[data-control-name="background_details_school"]',
                                         'type': 'attr', 'attr': 'href'}
FIELDS['PROFILE']['EDUCATION']['DEGREE'] = {'selector': '.pv-entity__degree-name',
                                            'type': 'text'}
FIELDS['PROFILE']['EDUCATION']['MAJOR'] = {'selector': '.pv-entity__fos', 'type': 'text'}
FIELDS['PROFILE']['EDUCATION']['GRADE'] = {'selector': '.pv-entity__grade', 'type': 'text'}
FIELDS['PROFILE']['EDUCATION']['DATE'] = {'selector': '.pv-entity__dates', 'type': 'text'}
FIELDS['PROFILE']['EDUCATION']['IMG'] = {'selector': '.pv-entity__logo img:not([class*=ghost])', 'type': 'attr', 'attr': 'src'}

SECTIONS['PROFILE']['CERTIFICATIONS'] = {'selector': '.pv-profile-section.certifications.pv-accomplishments-block--expanded ul > li', 'quantity': 'multiple'}
FIELDS['PROFILE']['CERTIFICATIONS'] = {}
FIELDS['PROFILE']['CERTIFICATIONS']['NAME'] = {'selector': '.pv-accomplishment-entity__title', 'type': 'text'}
FIELDS['PROFILE']['CERTIFICATIONS']['URL'] = {'selector': 'a.pv-accomplishment-entity__external-source', 'type': 'attr', 'attr': 'href'}
FIELDS['PROFILE']['CERTIFICATIONS']['DATE'] = {'selector': '.pv-accomplishment-entity__date', 'type': 'text'}
FIELDS['PROFILE']['CERTIFICATIONS']['AUTHORITY'] = {'selector': 'p:not([class$=subtitle])', 'type': 'text'}
FIELDS['PROFILE']['CERTIFICATIONS']['IMG'] = {'selector': '.pv-accomplishment-entity__photo img:not([class*=ghost])', 'type': 'attr', 'attr': 'src'}

SECTIONS['PROFILE']['ORGANIZATIONS'] = {'selector': '.pv-profile-section.organizations.pv-accomplishments-block--expanded ul > li', 'quantity': 'multiple'}
FIELDS['PROFILE']['ORGANIZATIONS'] = {}
FIELDS['PROFILE']['ORGANIZATIONS']['NAME'] = {'selector': '.pv-accomplishment-entity__title', 'type': 'text'}
FIELDS['PROFILE']['ORGANIZATIONS']['DATE'] = {'selector': '.pv-accomplishment-entity__date', 'type': 'text'}

SECTIONS['PROFILE']['PATENTS'] = {'selector': '.pv-profile-section.patents.pv-accomplishments-block--expanded ul > li', 'quantity': 'multiple'}
FIELDS['PROFILE']['PATENTS'] = {}
FIELDS['PROFILE']['PATENTS']['NAME'] = {'selector': '.pv-accomplishment-entity__title', 'type': 'text'}
FIELDS['PROFILE']['PATENTS']['DESCRIPTION'] = {'selector': '.pv-accomplishment-entity__description', 'type': 'text'}
FIELDS['PROFILE']['PATENTS']['ISSUER'] = {'selector': '.pv-accomplishment-entity__issuer', 'type': 'text'}
FIELDS['PROFILE']['PATENTS']['DATE'] = {'selector': '.pv-accomplishment-entity__date', 'type': 'text'}
FIELDS['PROFILE']['PATENTS']['CONTRIBUTOR_URL'] = {'selector': 'a[data-control-name="patent_contributors"]', 'type': 'attr', 'attr': 'href'}

SECTIONS['PROFILE']['SCORES'] = {'selector': '.pv-profile-section.testScores.pv-accomplishments-block--expanded ul > li', 'quantity': 'multiple'}
FIELDS['PROFILE']['SCORES'] = {}
FIELDS['PROFILE']['SCORES']['NAME'] = {'selector': '.pv-accomplishment-entity__title', 'type': 'text'}
FIELDS['PROFILE']['SCORES']['VALUE'] = {'selector': '.pv-accomplishment-entity__score', 'type': 'text'}
FIELDS['PROFILE']['SCORES']['DATE'] = {'selector': '.pv-accomplishment-entity__date', 'type': 'text'}