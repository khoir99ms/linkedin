CONTAINER = {'COMMENTS': '.feed-s-comments-list'}
SECTIONS = {'COMMENTS': {}}
FIELDS = {'COMMENTS': {}}

SECTIONS['COMMENTS']['POST_ACTOR'] = {'selector': 'article.feed-s-update .feed-s-post-meta__profile-link .feed-s-post-meta__name', 'type': 'text', 'quantity': 'single'}
SECTIONS['COMMENTS']['POST_ACTOR_URL'] = {'selector': 'article.feed-s-update .feed-s-post-meta__profile-link', 'type': 'attr', 'attr': 'href', 'quantity': 'single'}
SECTIONS['COMMENTS']['COMMENT'] = {'selector': '.feed-s-comments-list > article', 'quantity': 'multiple'}

FIELDS['COMMENTS']['COMMENT'] = {}
FIELDS['COMMENTS']['COMMENT']['COMMENT_ACTOR'] = {'selector': '.feed-s-comment-item__name:eq(0)', 'type': 'text'}
FIELDS['COMMENTS']['COMMENT']['COMMENT_ACTOR_URL'] = {'selector': '.feed-s-comment-item__inline-show-more-text > a.profile-link:eq(0)', 'type': 'attr', 'attr': 'href'}
FIELDS['COMMENTS']['COMMENT']['COMMENT_TEXT'] = {'selector': '.feed-s-main-content--comment:eq(0)', 'type': 'text'}
FIELDS['COMMENTS']['COMMENT']['COMMENT_DATE'] = {'selector': 'time.feed-s-comment-item__timestamp:eq(0)', 'type': 'text'}
FIELDS['COMMENTS']['COMMENT']['COMMENT_ID'] = {'selector': '', 'type': 'attr', 'attr': 'data-id'}
