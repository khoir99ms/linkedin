CONTAINER = {'SEARCH': '.results-list'}

SECTIONS = {'SEARCH': {}}
FIELDS = {'SEARCH': {}}

SECTIONS['SEARCH']['PROFILE'] = {'selector': '.results-list .search-result.search-result--person', 'quantity': 'multiple'}
FIELDS['SEARCH']['PROFILE'] = {}
FIELDS['SEARCH']['PROFILE']['NAME'] = {'selector': '.name-and-icon > .name', 'type': 'text'}
FIELDS['SEARCH']['PROFILE']['OCCUPATION'] = {'selector': 'p.subline-level-1', 'type': 'text'}
FIELDS['SEARCH']['PROFILE']['LOCATION'] = {'selector': 'p.subline-level-2', 'type': 'text'}
FIELDS['SEARCH']['PROFILE']['LINK'] = {'selector': 'a.search-result__result-link', 'type': 'attr', 'attr': 'href'}
FIELDS['SEARCH']['PROFILE']['PICTURE'] = {'selector': '.search-result__image > img', 'type': 'attr', 'attr': 'src'}
