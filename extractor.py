__author__ = 'Khoir MS'

import re
import urlparse

from pyquery import PyQuery as PyQ
from selector.profile import SECTIONS as PROFILE_SECTIONS, FIELDS as PROFILE_FIELDS
from selector.posts import SECTIONS as POST_SECTIONS, FIELDS as POST_FIELDS
from selector.comments import SECTIONS as COMMENT_SECTIONS, FIELDS as COMMENT_FIELDS
from selector.search import SECTIONS as SEARCH_SECTIONS, FIELDS as SEARCH_FIELDS

from logger import Logger
from utils.date import linkedin_date_fixer
from utils.html import html_entities, cleaner, strip_tags, remove_overtags

class Extractor(object):

    def __init__(self):
        self.logger = Logger(self.__class__.__name__)

        self.pyq = None
        self.document = None
        self.sections = None
        self.fields = None

    def set_document(self, doc, parser=None):
        self.document = doc
        self.pyq = PyQ(self.document, parser=parser)
        self.pyq.make_links_absolute('https://www.linkedin.com')
        self.pyq('.visually-hidden').remove()

    def extract_section(self, section, subsection):
        results = {}
        subsection_key = subsection.lower()
        container = self.sections[section][subsection]

        self.logger.log('container selector :', container['selector'])
        if container['quantity'] == 'multiple':
            results[subsection_key] = []
            elements = self.pyq(container['selector'])
            for element in elements:
                row = {}
                for field in self.fields[section][subsection]:
                    record = self.fields[section][subsection][field]
                    row[field.lower()] = self.get_field_value(record, element)
                results[subsection_key].append(row)
        elif container['quantity'] == 'single':
            results[subsection_key] = self.get_field_value(container)
        return results

    def generator_extract_section(self, section, subsection):
        container = self.sections[section][subsection]

        self.logger.log('container selector :', container['selector'])
        if container['quantity'] == 'multiple':
            elements = self.pyq(container['selector'])
            for element in elements:
                row = {}
                for field in self.fields[section][subsection]:
                    record = self.fields[section][subsection][field]
                    row[field.lower()] = self.get_field_value(record, element)
                yield row
        elif container['quantity'] == 'single':
            yield self.get_field_value(container)

    def get_field_value(self, record, parent=None):
        exit = None
        if not parent is None:
            if record['type'] == 'attr':
                if record['selector']:
                    exit = PyQ(parent, parser='soup')(record['selector']).attr(record['attr'])
                else:
                    exit = PyQ(parent, parser='soup').attr(record['attr'])
            elif record['type'] == 'text':
                if record['selector']:
                    exit = PyQ(parent, parser='soup')(record['selector']).text()
                else:
                    exit = PyQ(parent, parser='soup').text()
            elif record['type'] == 'html':
                if record['selector']:
                    exit = PyQ(parent, parser='soup')(record['selector']).html()
                else:
                    exit = PyQ(parent, parser='soup').html()
            elif record['type'] == 'style':
                exit = record['attr']
        else:
            if record['type'] == 'attr':
                exit = self.pyq(record['selector']).attr(record['attr'])
            elif record['type'] == 'text':
                exit = self.pyq(record['selector']).text()
            elif record['type'] == 'html':
                exit = self.pyq(record['selector']).html()
            elif record['type'] == 'style':
                exit = record['attr']
        if record['type'] in ['text', 'html']:
            exit = html_entities(exit)

        return exit

class Search(Extractor):

    def __init__(self):
        super(Search, self).__init__()
        self.sections = SEARCH_SECTIONS
        self.fields = SEARCH_FIELDS

    def profile(self):
        result = {}
        self.profiles = self.extract_section('SEARCH', 'PROFILE')
        result.update(self.profiles)

        return result

class Profile(Extractor):
    def __init__(self):
        super(Profile, self).__init__()
        self.sections = PROFILE_SECTIONS
        self.fields = PROFILE_FIELDS

    def basic(self):
        result = {}

        self.name = self.extract_section('BASIC_PROFILE', 'NAME')
        result.update(self.name)

        self.image = self.extract_section('BASIC_PROFILE', 'IMAGE')
        result.update(self.image)

        self.connections = self.extract_section('BASIC_PROFILE', 'CONNECTIONS')
        result.update(self.connections)

        self.title = self.extract_section('BASIC_PROFILE', 'TITLE')
        result.update(self.title)

        self.location = self.extract_section('BASIC_PROFILE', 'LOCATION')
        result.update(self.location)

        self.birthday = self.extract_section('BASIC_PROFILE', 'BIRTHDAY')
        result.update(self.birthday)

        self.profile_url = self.extract_section('BASIC_PROFILE', 'PROFILE_URL')
        result.update(self.profile_url)

        self.summary = self.extract_section('BASIC_PROFILE', 'SUMMARY')
        result.update(self.summary)

        self.followers = self.extract_section('BASIC_PROFILE', 'FOLLOWERS')
        if self.followers.get('followers'):
           self.followers['followers'] = re.sub(r'[^0-9]', '', self.followers['followers'])
        result.update(self.followers)

        self.email = self.extract_section('BASIC_PROFILE', 'EMAIL')
        result.update(self.email)

        self.ims = self.extract_section('BASIC_PROFILE', 'IMS')
        result.update(self.ims)

        self.phones = self.extract_section('BASIC_PROFILE', 'PHONES')
        result.update(self.phones)

        self.websites = self.extract_section('BASIC_PROFILE', 'WEBSITES')
        result.update(self.websites)

        self.twitter = self.extract_section('BASIC_PROFILE', 'TWITTER')
        result.update(self.twitter)

        return result

    def experience(self):
        result = {}
        self.experiences = self.extract_section('PROFILE', 'EXPERIENCE')
        result.update(self.experiences)

        return result

    def volunteer(self):
        result = {}
        self.volunteers = self.extract_section('PROFILE', 'VOLUNTEER')
        result.update(self.volunteers)

        return result

    def course(self):
        result = {}
        self.courses = self.extract_section('PROFILE', 'COURSES')
        result.update(self.courses)

        return result

    def publication(self):
        result = {}
        self.publications = self.extract_section('PROFILE', 'PUBLICATIONS')
        for i, publication in enumerate(self.publications.get('projects', [])):
            if not publication['description']:
                continue
            text = cleaner(publication['description'])
            text = strip_tags(text)
            text = remove_overtags(text)
            self.publications['publications'][i]['description'] = text
        result.update(self.publications)

        return result

    def project(self):
        result = {}
        self.projects = self.extract_section('PROFILE', 'PROJECTS')
        for i, project in enumerate(self.projects.get('projects', [])):
            if not project['description']:
                continue
            text = cleaner(project['description'])
            text = strip_tags(text)
            text = remove_overtags(text)
            self.projects['projects'][i]['description'] = text

        result.update(self.projects)

        return result

    def award(self):
        result = {}
        self.awards = self.extract_section('PROFILE', 'AWARDS')
        for i, award in enumerate(self.awards.get('awards', [])):
            if not award['description']:
                continue
            text = cleaner(award['description'])
            text = strip_tags(text)
            text = remove_overtags(text)
            self.awards['awards'][i]['description'] = text

        result.update(self.awards)

        return result

    def language(self):
        result = {}
        self.languages = self.extract_section('PROFILE', 'LANGUAGES')
        result.update(self.languages)

        return result

    def skill(self):
        result = {}
        self.skills = self.extract_section('PROFILE', 'SKILLS')
        result.update(self.skills)

        return result

    def education(self):
        result = {}
        self.educations = self.extract_section('PROFILE', 'EDUCATION')
        result.update(self.educations)

        return result

    def certification(self):
        result = {}
        self.certifications = self.extract_section('PROFILE', 'CERTIFICATIONS')
        result.update(self.certifications)

        return result

    def organization(self):
        result = {}
        self.organizations = self.extract_section('PROFILE', 'ORGANIZATIONS')
        result.update(self.organizations)

        return result

    def patent(self):
        result = {}
        self.patents = self.extract_section('PROFILE', 'PATENTS')
        result.update(self.patents)

        return result

    def score(self):
        result = {}
        self.scores = self.extract_section('PROFILE', 'SCORES')
        result.update(self.scores)

        return result

class Posts(Extractor):
    def __init__(self):
        super(Posts, self).__init__()
        self.sections = POST_SECTIONS
        self.fields = POST_FIELDS

    def post(self, mode):
        for post in self.generator_extract_section(mode, 'POST'):
            if post and isinstance(post, dict):
                # get user id
                if 'actor_url' in post and post['actor_url']:
                    uri_path = urlparse.urlsplit(post['actor_url']).path
                    uri_path = filter(lambda v: v, uri_path.split('/'))
                    post['actor_id'] = uri_path[-1]
                if 'date' in post and post['date']:
                    post['date'] = linkedin_date_fixer(post['date'])
                if 'image' in post and post['image']:
                    image = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', post['image'])
                    image = filter(lambda img: img, image)
                    if image: post['image'] = image[0]

                yield post

class Comments(Extractor):
    def __init__(self):
        super(Comments, self).__init__()
        self.sections = COMMENT_SECTIONS
        self.fields = COMMENT_FIELDS

    def comment(self):
        post_actor = self.extract_section('COMMENTS', 'POST_ACTOR')
        post_actor_url = self.extract_section('COMMENTS', 'POST_ACTOR_URL')
        # get user id
        if post_actor_url['post_actor_url']:
            uri_path = urlparse.urlsplit(post_actor_url['post_actor_url']).path
            uri_path = filter(lambda v: v, uri_path.split('/'))
            post_actor_id = uri_path[-1]
        else:
            post_actor_id = ''

        for comment in self.generator_extract_section('COMMENTS', 'COMMENT'):
            if comment and isinstance(comment, dict):
                comment.update(post_actor)
                comment.update(post_actor_url)

                comment['post_actor_id'] = post_actor_id
                # get user id
                if 'comment_actor_url' in comment and comment['comment_actor_url']:
                    uri_path = urlparse.urlsplit(comment['comment_actor_url']).path
                    uri_path = filter(lambda v: v, uri_path.split('/'))
                    comment['comment_actor_id'] = uri_path[-1]
                if comment['comment_date']:
                    comment['comment_date'] = linkedin_date_fixer(comment['comment_date'])

                yield comment


if __name__=='__main__':
    import json
    html = open('index.html').read()

    p = Profile()
    # p.set_document(html)
    p.set_document(html, 'soup')
    # print json.dumps(p.basic(), indent=4)
    # print json.dumps(p.experience(), indent=4)
    # print json.dumps(p.volunteer(), indent=4)
    # print json.dumps(p.course(), indent=4)
    # print json.dumps(p.publication(), indent=4)
    # print json.dumps(p.project(), indent=4) # description masih kosong jika parser tidak menggunakan soup
    # print json.dumps(p.award(), indent=4)
    # print json.dumps(p.language(), indent=4)
    # print json.dumps(p.skill(), indent=4)
    # print json.dumps(p.education(), indent=4)
    # print json.dumps(p.certification(), indent=4)
    # print json.dumps(p.organization(), indent=4)
    # print json.dumps(p.patent(), indent=4)
    # print json.dumps(p.score(), indent=4)